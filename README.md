# react-utility-components

> Awesome library to save developer time in perfroming repetitive tasks

[![NPM](https://img.shields.io/npm/v/react-utility-components.svg)](https://www.npmjs.com/package/react-utility-components) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save react-utility-components
```

## Usage

```jsx
import React, { Component } from 'react'

import MyComponent from 'react-utility-components'

class Example extends Component {
  render () {
    return (
      <MyComponent />
    )
  }
}
```

## License

MIT © [sunnyofhell](https://github.com/sunnyofhell)
